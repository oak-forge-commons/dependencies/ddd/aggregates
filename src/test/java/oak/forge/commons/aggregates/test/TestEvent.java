package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Event;

import java.time.Instant;
import java.util.UUID;

import static oak.forge.commons.data.version.Version.of;

public class TestEvent extends Event {

    private static final long serialVersionUID = -1381892892159632149L;

    static final UUID CREATOR_ID = UUID.randomUUID();

    public TestEvent() {
    }

    public TestEvent(UUID aggregateId) {
        super(aggregateId, CREATOR_ID);
    }

    public TestEvent(UUID aggregateId, int version) {
        super(aggregateId, CREATOR_ID);
        setVersion(of(version));
    }

    public TestEvent(UUID aggregateId, UUID creatorId) {
        super(aggregateId, creatorId);
    }

    public TestEvent(UUID aggregateId, int version, UUID creatorId) {
        super(aggregateId, creatorId);
        setVersion(of(version));
    }
}
