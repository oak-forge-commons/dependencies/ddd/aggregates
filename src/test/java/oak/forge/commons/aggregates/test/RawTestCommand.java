package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Command;

/**
 * Test command class used for verifying command preparation mechanism.
 */
public class RawTestCommand extends Command {

    private static final long serialVersionUID = -2228432523016590578L;

    private boolean prepared;

    public RawTestCommand() {
    }

    public void prepare() {
        prepared = true;
    }

    public boolean isPrepared() {
        return prepared;
    }
}
