package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

public class GenerateMultipleEvents extends Command {

    private static final long serialVersionUID = 1L;

    public GenerateMultipleEvents(UUID aggregateId) {
        super(aggregateId);
        withName(getClass().getSimpleName());
    }
}
