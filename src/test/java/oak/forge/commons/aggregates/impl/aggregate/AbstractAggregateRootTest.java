package oak.forge.commons.aggregates.impl.aggregate;

import oak.forge.commons.aggregates.api.aggregate.*;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.version.Version;
import oak.forge.commons.aggregates.test.*;
import oak.forge.commons.aggregates.testutils.EventStreamBuilder;
import oak.forge.commons.aggregates.testutils.Events;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static java.time.Instant.now;
import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;

public class AbstractAggregateRootTest {

    private static final String COMMAND_NAME = "command-name";
    private static final String EVENT_DATA = "event-data";
    private static final UUID AGGREGATE_ID = randomUUID();
    private static final UUID CREATOR_ID = randomUUID();

    class State extends AggregateState {
        private static final long serialVersionUID = 1L;

        State(UUID aggregateId) {
            super(aggregateId);
        }
    }

    private final Context context = mock(Context.class);
    private final EventStore eventStore = mock(EventStore.class);
    private final AggregateLoader aggregateLoader = mock(AggregateLoader.class);
    private final MessageBus messageBus = mock(MessageBus.class);

    private AbstractAggregateRoot<State> getAggregateRoot() {
        return new AbstractAggregateRoot<State>(messageBus, eventStore, aggregateLoader, context) {
            @Override
            public State initState(UUID aggregateId) {
                return new State(aggregateId);
            }
        };
    }

    private TestAggregate getTestAggregate() {
        TestAggregate aggregateRoot = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
        aggregateRoot.attachState(new TestAggregate.State(AGGREGATE_ID));
        return aggregateRoot;
    }

    private TestAggregate getTestAggregateNotInitialized() {
        TestAggregate aggregateRoot = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
        aggregateRoot.attachState(new TestAggregate.State(null));
        return aggregateRoot;
    }

    @Test
    public void should_throw_when_cannot_find_command_handler() {
        // given
        AbstractAggregateRoot<State> aggregateRoot = getAggregateRoot();

        // then
        assertThatExceptionOfType(NoSuchAggregateCommandHandlerException.class)
                .isThrownBy(() -> aggregateRoot.handleCommand(new TestCommand()));
    }

    @Test
    public void should_handle_command_generating_single_event() {
        // given
        TestAggregate aggregateRoot = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
        aggregateRoot.attachState(new TestAggregate.State(AGGREGATE_ID, Version.of(2)));
        TestCommand command = new TestCommand(COMMAND_NAME, AGGREGATE_ID);

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        assertThat(events.source()).hasSize(1);
        TestEvent event = events.next();
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getVersion()).isEqualTo(Version.of(3));
    }

    @Test
    public void should_handle_command_generating_multiple_events() {
        // given
        TestAggregate aggregateRoot = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
        aggregateRoot.attachState(new TestAggregate.State(AGGREGATE_ID, Version.of(2)));
        GenerateMultipleEvents command = new GenerateMultipleEvents(AGGREGATE_ID);

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        assertThat(events.source()).hasSize(3);
        TestEvent event = events.skip(2).next();
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getVersion()).isEqualTo(Version.of(5));
    }

    @Test
    public void should_throw_when_cannot_find_event_handler() {
        // given
        AbstractAggregateRoot<State> aggregateRoot = getAggregateRoot();
        List<Event> eventStream = new ArrayList<>();
        eventStream.add(new TestEvent(AGGREGATE_ID, CREATOR_ID));

        // then
        assertThatExceptionOfType(NoSuchAggregateEventHandlerException.class)
                .isThrownBy(() -> aggregateRoot.handleEvents(eventStream));
    }

    @Test
    public void should_handle_event_stream() {
        // given
        TestAggregate aggregateRoot = getTestAggregate();
        List<Event> eventStream = EventStreamBuilder.eventStream(
                new TestEvent(AGGREGATE_ID, 1, CREATOR_ID)
        ).get();

        // when
        aggregateRoot.handleEvents(eventStream);

        // then
        Assertions.assertThat(aggregateRoot.getState().string).isEqualTo(AGGREGATE_ID.toString());
    }

    @Test
    public void should_throw_when_handling_event_of_same_version_as_aggregate() {
        // given
        TestAggregate aggregateRoot = getTestAggregate(); // version: 0
        List<Event> eventStream = singletonList(new TestEvent(AGGREGATE_ID, CREATOR_ID));

        // then
        assertThatExceptionOfType(ConcurrentAggregateModificationException.class)
                .isThrownBy(() -> aggregateRoot.handleEvents(eventStream));
    }

    @Test
    public void should_throw_when_handling_event_of_same_version_more_than_once() {
        // given
        TestAggregate aggregateRoot = getTestAggregate(); // version: 0
        List<Event> eventStream = Arrays.asList(new TestEvent(AGGREGATE_ID, 1), new TestEvent(AGGREGATE_ID, 1));

        // then
        assertThatExceptionOfType(ConcurrentAggregateModificationException.class)
                .isThrownBy(() -> aggregateRoot.handleEvents(eventStream));
    }

    @Test
    public void should_throw_when_handling_event_of_lower_version() {
        // given
        TestAggregate aggregateRoot = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
        aggregateRoot.attachState(new TestAggregate.State(AGGREGATE_ID, Version.of(2)));
        List<Event> eventStream = singletonList(new TestEvent(AGGREGATE_ID, 1, CREATOR_ID));

        // then
        assertThatExceptionOfType(ConcurrentAggregateModificationException.class)
                .isThrownBy(() -> aggregateRoot.handleEvents(eventStream));
    }

    @Test
    public void should_handle_command_outside_of_process() {
        // given
        TestAggregate aggregateRoot = getTestAggregate();
        TestCommand command = new TestCommand(COMMAND_NAME, AGGREGATE_ID);

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        TestEvent event = events.next();
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getCorrelationId()).isNull();
    }

    @Test
    public void should_handle_command_within_process() {
        // given
        TestAggregate aggregateRoot = getTestAggregate();
        TestCommand command = new TestCommand(COMMAND_NAME, AGGREGATE_ID);
        UUID correlationId = randomUUID();
        command.setCorrelationId(correlationId);

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        TestEvent event = events.next();
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getCorrelationId()).isEqualTo(correlationId);
    }

    @Test
    public void should_not_override_process_id_in_event_if_command_does_not_specify_one() {
        // given
        UUID correlationId = randomUUID();
        TestAggregate aggregateRoot = getTestAggregate();
        aggregateRoot.simulateProcess(correlationId);
        TestCommand command = new TestCommand(COMMAND_NAME, AGGREGATE_ID);

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        TestEvent event = events.next();
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getCorrelationId()).isEqualTo(correlationId);
    }

    @Test
    public void should_set_common_properties_in_event_even_if_command_handler_misses_them() {
        // given
        TestAggregate aggregateRoot = getTestAggregate();
        UpdateCommonProperties command = new UpdateCommonProperties(AGGREGATE_ID, CREATOR_ID);
        Instant before = now();

        // when
        Events events = Events.of(aggregateRoot.handleCommand(command));

        // then
        assertThat(events.source()).hasSize(1);
        TestEvent event = events.next();
        assertThat(event.getId()).isNotNull();
        assertThat(event.getId()).isNotEqualTo(AGGREGATE_ID);
        assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
        assertThat(event.getCreatorId()).isEqualTo(CREATOR_ID);
    }

    @Test
    public void should_throw_if_cannot_determine_aggregate_id_after_command_handler_invocation() {
        // given
        TestAggregate aggregateRoot = getTestAggregateNotInitialized();
        UpdateCommonProperties command = new UpdateCommonProperties();

        // then
        assertThatExceptionOfType(AggregateStateException.class)
                .isThrownBy(() -> aggregateRoot.handleCommand(command))
                .withMessageContaining("aggregateId")
                .withMessageContaining(UpdateCommonProperties.class.getSimpleName());
    }

    @Test
    public void should_throw_if_cannot_determine_creator_id_after_command_handler_invocation() {
        // given
        TestAggregate aggregateRoot = getTestAggregateNotInitialized();
        UpdateCommonProperties command = new UpdateCommonProperties(AGGREGATE_ID, null);

        // then
        assertThatExceptionOfType(AggregateStateException.class)
                .isThrownBy(() -> aggregateRoot.handleCommand(command))
                .withMessageContaining("creatorId")
                .withMessageContaining(UpdateCommonProperties.class.getSimpleName());
    }

}
