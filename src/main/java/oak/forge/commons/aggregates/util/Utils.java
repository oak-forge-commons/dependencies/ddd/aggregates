package oak.forge.commons.aggregates.util;

import java.lang.reflect.ParameterizedType;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

public final class Utils {

	private Utils() {
	}

	public static boolean isEmpty(String s) {
		return s == null || s.trim().isEmpty();
	}

	public static boolean isNotEmpty(String s) {
		return !isEmpty(s);
	}

	public static boolean isEmpty(String[] s) {
		return s == null || s.length == 0 || Stream.of(s).noneMatch(Utils::isNotEmpty);
	}

	public static void checkArgument(boolean expression, Object errorMessage) {
		if (!expression) {
			throw new IllegalArgumentException(String.valueOf(errorMessage));
		}
	}

	public static <T> Class<T> getGenericArgumentType(Class<?> clazz, int typeArgumentIndex) {
		return (Class<T>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[typeArgumentIndex];
	}

	public static String anonymize(String value) {
		return isEmpty(value) ? "<empty>" : "<set>";
	}

	public static <T> T findFirstOrThrow(Supplier<RuntimeException> exceptionSupplier, Supplier<T>... values) {
		return Stream.of(values)
				.map(Supplier::get)
				.filter(Objects::nonNull)
				.findFirst()
				.orElseThrow(exceptionSupplier);
	}
}
