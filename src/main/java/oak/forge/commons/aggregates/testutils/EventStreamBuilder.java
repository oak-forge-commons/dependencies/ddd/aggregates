package oak.forge.commons.aggregates.testutils;

import oak.forge.commons.data.message.Event;

import java.util.ArrayList;
import java.util.List;

public class EventStreamBuilder {

	private List<Event> stream;
	private int version;

	private EventStreamBuilder(Event[] events) {
		reset();
		for (Event event : events) {
			event.updateVersion(version++);
			stream.add(event);
		}
	}

	private EventStreamBuilder reset() {
		version = 1;
		stream = new ArrayList<>();
		return this;
	}

	public List<Event> get() {
		return stream;
	}

	public static EventStreamBuilder eventStream(Event... events) {
		return new EventStreamBuilder(events);
	}
}
