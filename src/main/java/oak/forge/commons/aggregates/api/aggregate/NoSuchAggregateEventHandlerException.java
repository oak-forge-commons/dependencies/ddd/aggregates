package oak.forge.commons.aggregates.api.aggregate;

public class NoSuchAggregateEventHandlerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoSuchAggregateEventHandlerException(String message) {
		super(message);
	}
}
