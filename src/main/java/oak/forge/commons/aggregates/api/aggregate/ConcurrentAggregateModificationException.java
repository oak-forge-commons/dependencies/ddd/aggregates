package oak.forge.commons.aggregates.api.aggregate;

public class ConcurrentAggregateModificationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConcurrentAggregateModificationException(String message) {
		super(message);
	}
}
