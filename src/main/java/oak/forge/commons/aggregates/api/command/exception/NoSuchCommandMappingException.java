package oak.forge.commons.aggregates.api.command.exception;

public class NoSuchCommandMappingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoSuchCommandMappingException(String commandName) {
		super(commandName);
	}
}
