package oak.forge.commons.aggregates.api.aggregate;

import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.Event;

import java.util.List;
import java.util.UUID;

public interface AggregateRoot<S extends AggregateState> {

    default AggregateRoot<S> init(UUID aggregateId) {
        attachState(initState(aggregateId));
        return this;
    }

    S initState(UUID aggregateId);

    S getState();

    void attachState(S state);

    void detachState();

    <C extends Command> void prepareCommand(C command);

    <C extends Command> List<Event> handleCommand(C command);

    void handleEvent(Event event);

    default void handleEvents(List<Event> eventStream) {
        eventStream.forEach(this::handleEvent);
    }
}
