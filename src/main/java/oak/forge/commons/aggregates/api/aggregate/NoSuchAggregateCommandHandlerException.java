package oak.forge.commons.aggregates.api.aggregate;

public class NoSuchAggregateCommandHandlerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoSuchAggregateCommandHandlerException(String message) {
		super(message);
	}
}
