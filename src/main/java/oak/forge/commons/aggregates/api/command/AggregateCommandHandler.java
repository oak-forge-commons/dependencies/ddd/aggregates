package oak.forge.commons.aggregates.api.command;

import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.Event;

import java.util.List;

@FunctionalInterface
public interface AggregateCommandHandler<C extends Command, S extends AggregateState> {

    List<Event> handle(C command, S state);
}
