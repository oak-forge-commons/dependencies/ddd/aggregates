package oak.forge.commons.aggregates.api.command.exception;

public class CommandProcessingException extends RuntimeException {

	public CommandProcessingException(String msg) {
		super(msg);
	}

	public CommandProcessingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
