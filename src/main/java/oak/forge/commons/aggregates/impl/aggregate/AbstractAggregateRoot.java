package oak.forge.commons.aggregates.impl.aggregate;

import oak.forge.commons.aggregates.api.aggregate.*;
import oak.forge.commons.aggregates.api.command.AggregateCommandHandler;
import oak.forge.commons.aggregates.api.command.CommandPreparer;
import oak.forge.commons.aggregates.api.command.CommandValidator;
import oak.forge.commons.aggregates.api.command.exception.CommandProcessingException;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;

import oak.forge.commons.data.message.Event;
import oak.forge.commons.aggregates.util.Utils;
import oak.forge.commons.eventsourcing.api.event.AggregateEventHandler;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static java.util.UUID.randomUUID;


public abstract class AbstractAggregateRoot<S extends AggregateState> implements AggregateRoot<S> {

    private final Logger logger = LoggerFactory.getLogger(AbstractAggregateRoot.class);

    private final Map<Class<?>, CommandPreparer> commandPreparers;
    private final Map<Class<?>, AggregateCommandHandler> commandHandlers;
    private final Map<Class<?>, List<CommandValidator>> commandValidators;
    private final Map<Class<?>, AggregateEventHandler> aggregateEventHandlers;

    private final EventStore eventStore;
    private final AggregateLoader aggregateLoader;
    private final Context context;

    private S state;

    protected AbstractAggregateRoot(MessageBus messageBus, EventStore eventStore, AggregateLoader aggregateLoader, Context context) {
        this.eventStore = eventStore;
        this.aggregateLoader = aggregateLoader;
        this.context = context;
        this.commandPreparers = new HashMap<>();
        this.commandHandlers = new HashMap<>();
        this.commandValidators = new HashMap<>();
        this.aggregateEventHandlers = new HashMap<>();

        messageBus.registerMessageHandler(Command.class, this::processCommand);
        messageBus.registerMessageHandler(Event.class, this::handleEvent);

    }

    @Override
    public S getState() {
        return state;
    }

    @Override
    public void attachState(S state) {
        this.state = state;
    }

    @Override
    public void detachState() {
        this.state = null;
    }

    @Override
    public <C extends Command> void prepareCommand(C command) {
        ofNullable(commandPreparers.get(command.getClass()))
                .ifPresent(commandPreparer -> commandPreparer.prepare(command));
    }

    protected <C extends Command> void registerCommandPreparer(final Class<C> commandClass, final CommandPreparer<C> commandPreparer) {
        commandPreparers.put(commandClass, commandPreparer);
    }

    protected <C extends Command> void registerCommandValidator(final Class<C> commandClass,
                                                                final CommandValidator<C, S> commandValidator) {
        commandValidators.computeIfAbsent(commandClass, k -> new ArrayList<>()).add(commandValidator);
    }

    private <C extends Command> List<CommandValidator> getCommandValidators(final Class<C> commandClass) {
        return commandValidators.computeIfAbsent(commandClass, k -> new ArrayList<>());
    }

    protected <C extends Command> void registerCommandHandler(Class<C> commandClass, AggregateCommandHandler<C, S> aggregateCommandHandler) {
        commandHandlers.put(commandClass, aggregateCommandHandler);
    }

    protected <E extends Event> void registerEventHandler(Class<E> eventClass, AggregateEventHandler<E, S> eventHandler) {
        aggregateEventHandlers.put(eventClass, eventHandler);
    }

    public <C extends Command> void processCommand(C command) {
        AggregateRoot<?> aggregateRoot = null;
        aggregateRoot = aggregateLoader.load(command, eventStore, context);
        aggregateRoot.handleCommand(command);
    }

    @Override
    public <C extends Command> List<Event> handleCommand(C command) {

        List<Event> validationErrors = validateCommand(command, state);
        if (!validationErrors.isEmpty()) return validationErrors;

        AggregateCommandHandler<C, S> aggregateCommandHandler = ofNullable((AggregateCommandHandler<C, S>) commandHandlers.get(command.getClass()))
                .orElseThrow(() -> new NoSuchAggregateCommandHandlerException(format("%s: cannot find command handler for [%s]", getClass(), command.getName())));
        List<Event> events = aggregateCommandHandler.handle(command, getState());

        update(command, events, state.getVersion().get());
        if (events.isEmpty()) {
            String message = format("command handler for [%s] produced empty events list; please review implementation", command);
            throw new CommandProcessingException(message);
        }
        eventStore.store(events);
        return events;
    }

    private <C extends Command> List<Event> validateCommand(C command, S state) {
        for (CommandValidator validator : getCommandValidators(command.getClass())) {
            List<Event> validationErrors = validator.validate(command, state);
            if (!validationErrors.isEmpty()) {
                update(command, validationErrors, state.getVersion().get());
                eventStore.store(validationErrors);
                return validationErrors;
            }
        }
        return new ArrayList<>();
    }

    private <C extends Command> void update(C command, List<Event> events, int version) {
        for (Event event : events) {
            if (event.getAggregateId() == null) {
                UUID aggregateId = Utils.findFirstOrThrow(
                        () -> new AggregateStateException(format("cannot determine aggregateId after handling command: %s", command)),
                        command::getAggregateId,
                        () -> getState().getAggregateId()
                );
                event.setAggregateId(aggregateId);
            }
            if (event.getCreatorId() == null) {
                UUID creatorId = Utils.findFirstOrThrow(
                        () -> new AggregateStateException(format("cannot determine creatorId after handling command: %s", command)),
                        command::getCreatorId
                );
                event.setCreatorId(creatorId);
            }
            if (event.getId() == null) {
                event.setId(randomUUID());
            }
            if (command.getCorrelationId() != null) {
                event.correlatingTo(command.getCorrelationId());
            }

            event.updateVersion(++version);
        }
    }

    @Override
    public void handleEvent(Event event) {
        AggregateEventHandler aggregateEventHandler = ofNullable(aggregateEventHandlers.get(event.getClass()))
                .orElseThrow(() -> new NoSuchAggregateEventHandlerException(format("%s: cannot find event handler for [%s]", getClass(), event.getClass())));
        if (state.getVersion().compareTo(event.getVersion()) >= 0) {
            String message = format("aggregate version: [%s] is higher or equal current event version: [%s]", state.getVersion(), event.getVersion());
            throw new ConcurrentAggregateModificationException(message);
        }
        aggregateEventHandler.handle(event, getState());
        state.setVersion(event.getVersion());
    }


}
